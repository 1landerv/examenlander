
import React, {useEffect} from 'react';
import { CardImage } from "../pageStyles/pageStyles"
import { Card, CardHeader, Typography, Grid } from '@material-ui/core';

import BatteryChargingFullIcon from '@material-ui/icons/BatteryChargingFull';
import MemoryIcon from '@material-ui/icons/Memory';
import CameraRearIcon from '@material-ui/icons/CameraRear';
import CameraFrontIcon from '@material-ui/icons/CameraFront';
import PhoneAndroidIcon from '@material-ui/icons/PhoneAndroid';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import InsertDriveFileIcon from '@material-ui/icons/InsertDriveFile';

export default function PhoneView(props){

  useEffect(() => {
    console.log(props.phone.PhoneMeta)
  });
  
  return(
    <Card style={{marginBottom: 20, padding: 10}}>
      <CardHeader
        title={props.phone.PhoneMeta.phoneName}
        subheader={props.phone.PhoneMeta.releaseDate}
      />
      <Typography style={{marginBottom: 20}}>
        {props.phone.PhoneMeta.phoneDescription}
      </Typography>
      <Grid container>
        <Grid item>
          {<CardImage fluid={props.phone.PhoneMeta.phonePhoto.imageFile.childImageSharp.fluid} alt={props.phone.PhoneMeta.phonePhoto.altText} />}
        </Grid>
        <Grid item>
          <Typography><BatteryChargingFullIcon />{props.phone.PhoneMeta.battery}mAh</Typography>
          <Typography><MemoryIcon />{props.phone.PhoneMeta.chipset}</Typography>
          <Typography><MemoryIcon />{props.phone.PhoneMeta.ram}GB RAM</Typography>
          <Typography><CameraRearIcon />{props.phone.PhoneMeta.mainCamera}MP</Typography>
          <Typography><CameraFrontIcon />{props.phone.PhoneMeta.selfieCamera}MP</Typography>
          <Typography><PhoneAndroidIcon />{props.phone.PhoneMeta.operatingSystem}</Typography>
          <Typography><InsertDriveFileIcon />{props.phone.PhoneMeta.storage}</Typography>
          <Typography><CheckBoxOutlineBlankIcon />{props.phone.PhoneMeta.screenSize}"</Typography>
        </Grid>
      </Grid>
    </Card>
  );
}