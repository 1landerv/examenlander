import styled from "styled-components"
import { COLORS, FONT_FAMILIES, MEDIA_QUERIES } from "../../../constants"

export const HeaderWrapper = styled.header`
  display: flex;
  left: 0;
  top: 0;
  width: 100%;
  height: 80px;
  background: ${COLORS.BLUE};
  justify-content: space-between;
  align-items: center;
  padding: 0 5%;
  z-index: 999;
  a {
    display: flex;
    width: 200px;
    height: 80%;
    font-size: 2.2rem;
    text-align: center;
    justify-content: center;
    text-decoration: none;
    font-family: ${FONT_FAMILIES.BUTTON};
    color: ${COLORS.WHITE};
  }
  @media (max-width: ${MEDIA_QUERIES.MEDIUM}) {
    display: none;
  }
`

export const MenuList = styled.ul`
  display: flex;
  margin: 0;
  list-style: none;
  li {
    display: flex;
    justify-content: center;
    align-items: center;
    a {
      color: ${COLORS.WHITE};
      font-size: 1.5rem;
      font-family: ${FONT_FAMILIES.BUTTON};
      text-transform: uppercase;
      white-space: nowrap;
      text-decoration: none;
      padding: 0.25rem 1rem;
    }
    a:hover {
      color: ${COLORS.TERTIARY};
    }
  }
  .nav-active {
    color: ${COLORS.TERTIARY};
  }
`