import { Link, useStaticQuery} from "gatsby"
import PropTypes from "prop-types"
import React from "react"
import { HeaderWrapper } from "./headerStyles/headerStyles"
import Menu from "./menu"

const Header = ({ siteTitle }) => {
  const {
    wpcontent: { menuItems },
  } = useStaticQuery(graphql`
    query {
      wpcontent {
        menuItems {
          edges {
            node {
              label
              path
            }
          }
        }
      }
    }
  `)
  
  return(
    <HeaderWrapper>
      <Link to="/">
        {siteTitle}
      </Link>
      <Menu menuItems={menuItems.edges} />
    </HeaderWrapper>
  )
}

Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
}

export default Header
