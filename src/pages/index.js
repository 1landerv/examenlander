import React from "react"
import { useStaticQuery} from "gatsby"
import PhoneView from '../components/phoneview'
import Layout from "../components/layout"
import SEO from "../components/seo"
import { Image } from "../pageStyles/pageStyles"
import Typography from '@material-ui/core/Typography';

const IndexPage = () => {
  const {
    wpcontent: {
      page: {
        HomeMeta: {
          homePageDescription,
          homePageHeaderTitle,
          homePageBannerPhoto,
          homePageFeaturedProduct
        }
      }
    }
  } = useStaticQuery(graphql`
    query{
      wpcontent {
        page(id: "Home", idType: URI) {
          HomeMeta {
            homePageHeaderTitle
            homePageDescription
            homePageBannerPhoto {
              sourceUrl
              altText
              imageFile {
                childImageSharp {
                  fluid(quality: 100){
                    ...GatsbyImageSharpFluid_withWebp
                  }
                }
              }
            }
            homePageFeaturedProduct {
              ... on WPGraphql_Phone {
                id
                PhoneMeta {
                  battery
                  chipset
                  mainCamera
                  operatingSystem
                  phoneDescription
                  phoneName
                  ram
                  releaseDate
                  screenSize
                  selfieCamera
                  storage
                  phonePhoto {
                    sourceUrl
                    altText
                    imageFile {
                      childImageSharp {
                        fluid(quality: 100){
                          ...GatsbyImageSharpFluid_withWebp
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  `);

  return(
    <Layout>
      <SEO title="Home" />
        <Typography variant="h4">{homePageHeaderTitle}</Typography>
        <Image fluid={homePageBannerPhoto.imageFile.childImageSharp.fluid} alt={homePageBannerPhoto.altText} />
        <Typography variant="body1">{homePageDescription}</Typography>
        <Typography variant="h5">Featured Phone!</Typography>
        <PhoneView phone={homePageFeaturedProduct}></PhoneView>
    </Layout>
  )
}

export default IndexPage
