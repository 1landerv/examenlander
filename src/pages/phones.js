import React from "react"
import { useStaticQuery } from "gatsby"
import PhoneView from '../components/phoneview'
import Layout from "../components/layout"
import SEO from "../components/seo"
import { Image } from "../pageStyles/pageStyles"
import Typography from '@material-ui/core/Typography';

const PhonesPage = () => {
  const {
    wpcontent: {
      page: {
        PhonesMeta: {
          phonePageBannerPhoto,
          phonePageDescription
        }
      },
      phones
    }
  } = useStaticQuery(graphql`
    query{
      wpcontent {
        page(id: "Phones", idType: URI) {
          PhonesMeta {
            phonePageBannerPhoto {
              altText
              sourceUrl
              imageFile {
                childImageSharp {
                  fluid(quality: 100){
                    ...GatsbyImageSharpFluid_withWebp
                  }
                }
              }
            }
            phonePageDescription
          }
        }
        phones {
          nodes {
            PhoneMeta {
              battery
              chipset
              fieldGroupName
              mainCamera
              operatingSystem
              phoneDescription
              phoneName
              ram
              releaseDate
              screenSize
              selfieCamera
              storage
              phonePhoto {
                sourceUrl
                altText
                imageFile {
                  childImageSharp {
                    fluid(quality: 100){
                      ...GatsbyImageSharpFluid_withWebp
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  `);

  return(
    <Layout>
      <SEO title="Phones" />
      <Image fluid={phonePageBannerPhoto.imageFile.childImageSharp.fluid} alt={phonePageBannerPhoto.altText} />
      <Typography variant="body1">{phonePageDescription}</Typography>
      {phones.nodes.map((phone, slug) => (
        <PhoneView phone={phone} slug={slug}/>
      ))}
    </Layout>
  )
}

export default PhonesPage
